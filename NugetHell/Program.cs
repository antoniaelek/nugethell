﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NJsonSchema;
using Unitfly.ExtensionKit.VAF;

namespace NugetHell
{
    //public class Config
    //{
    //    public string Prop { get; set; }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            var s = GenerateSchema(typeof(Configuration));
            File.WriteAllText(@"C:\Users\antonia\Desktop\schema.json", s);
            Console.WriteLine(s);
        }

        public static string GenerateSchema(Type configType)
        {
            try
            {
                var schema = JsonSchema.FromType(configType);
                return schema.ToJson();
                //var schemaGenerator = new JSchemaGenerator();
                //schemaGenerator.GenerationProviders.Add(new StringEnumGenerationProvider());
                //schemaGenerator.GenerationProviders.Add(new CustomTypesGenerationProvider());
                //schemaGenerator.SchemaPropertyOrderHandling = SchemaPropertyOrderHandling.Alphabetical;
                //return schemaGenerator.Generate(configType);
            }
            catch (Exception e)
            {
                throw new Exception($"Error generating schema for application configuration.", e);
            }
        }
    }
}
